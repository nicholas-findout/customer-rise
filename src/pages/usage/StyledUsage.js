import styled from 'styled-components';

const StyledUsage = styled.div`
    display: flex;
    flex-direction: column;
    padding: 20px 10px;
    align-items: center;
`

export default StyledUsage;