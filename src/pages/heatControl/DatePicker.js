import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';

import StyledDatePicker from 'pages/heatControl/datePicker/StyledDatePicker';
import { setFocusedDateFrom, setFocusedDateTo } from 'modules/data/actions';

const DatePicker = ({ numberOfMonths, from, to, setFrom, setTo }) => {

    const handleDayClick = (day) => {
        const range = DateUtils.addDayToRange(day, { from, to });
        setFrom(range.from);
        setTo(range.to);
    }

    const MONTHS = ['Januari', 'Februari', 'Mars', 'April', 'Maj', 'Juni', 'Juli', 'Augusti', 'September', 'Oktober', 'November', 'December'];

    const WEEKDAYS_LONG = ['Söndag', 'Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag'];

    const WEEKDAYS_SHORT = ['Sö', 'Må', 'Ti', 'On', 'To', 'Fr', 'Lö'];

    const modifiers = { start: from, end: to };

    return (
        <StyledDatePicker>
            <DayPicker
                className="Selectable"
                numberOfMonths={numberOfMonths}
                locale="se"
                selectedDays={[from, { from, to }]}
                modifiers={modifiers}
                onDayClick={handleDayClick}
                firstDayOfWeek={1}
                months={MONTHS}
                weekdaysLong={WEEKDAYS_LONG}
                weekdaysShort={WEEKDAYS_SHORT}
            />
            <Helmet />
        </StyledDatePicker>
    );
}

const mapStateToProps = state => {
    const { focusedDate: { from, to } } = state.data.data;
    return { from, to }
}

const mapDispatchToProps = dispatch => {
    return {
        setFrom: from => dispatch(setFocusedDateFrom(from)),
        setTo: to => dispatch(setFocusedDateTo(to)),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DatePicker);