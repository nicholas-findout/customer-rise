import styled from 'styled-components';

const StyledDatePicker = styled.div`

    *:focus {
        outline: none;
    }

    .Selectable {
        .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
            background-color: #f0f8ff;
            color: #4a90e2;
        }
        
        .DayPicker-Day {
            border-radius: 0;
        }
        
        .DayPicker-Day--start {
            border-top-left-radius: 100%;
            border-bottom-left-radius: 100%;
        }
        
        .DayPicker-Day--end {
            border-top-right-radius: 100%;
            border-bottom-right-radius: 100%;
        }

        &.DayPicker {
            width: 100%;

            .DayPicker-Month {
                width: 100%;
            }
        }
    }
`;

export default StyledDatePicker;