import { createSelector } from 'reselect'
import { get } from 'lodash'

const userGoals = state => get(state, 'data.data.userGoals');

const heatControlId = state => get(state, 'data.data.currentHeatControlId');

export const getUserGoals = createSelector(
    userGoals,
    userGoals => userGoals
);