import { combineReducers } from 'redux';

import data from 'modules/data/reducers';

const appReducer = combineReducers({
    data
});

export default appReducer;
