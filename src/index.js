import React from 'react';
import ReactDOM from 'react-dom';

import Root from 'Root';
import configureStore from 'modules/store'

export const store = configureStore()

ReactDOM.render(<Root store={store} />, document.getElementById('root'));